<?php

namespace Imoje\Blik\Block;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Urlnotification
 *
 * @package Imoje\Blik\Block
 */
class Urlnotification extends Field
{

	/**
	 * @param AbstractElement $element
	 *
	 * @return string
	 */
	protected function _getElementHtml(AbstractElement $element)
	{
		return $this->getBaseUrl() . 'imoje_blik/payment/notification';
	}

}
