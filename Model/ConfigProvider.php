<?php

namespace Imoje\Blik\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data;

/**
 * Class ConfigProvider
 *
 * @package Imoje\Blik\Model
 */
class ConfigProvider implements ConfigProviderInterface
{
	const PP_CODE = 'imoje_blik';

	/**
	 * @var \Imoje\Blik\Model\Blik
	 */
	protected $blikMethod;

	/**
	 * Payment ConfigProvider constructor.
	 *
	 * @param Data $paymentHelper
	 *
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function __construct(Data $paymentHelper)
	{
		$this->blikMethod = $paymentHelper->getMethodInstance(self::PP_CODE);
	}

	/**
	 * Retrieve assoc array of checkout configuration
	 *
	 * @return array
	 */
	public function getConfig()
	{
		$config = [];

		if($this->blikMethod->isAvailable()) {
			$config = [
				'payment' => [
					self::PP_CODE => [
						'redirectUrl'    => $this->blikMethod->getCheckoutRedirectUrl(),
						'paymentLogoSrc' => $this->blikMethod->getPaymentLogoSrc(),
					],
				],
			];
		}

		return $config;
	}
}
