<?php

namespace Imoje\Blik\Block;

include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/autoload.php";

use Imoje\Blik\Model\Order;
use Imoje\Payment\Api;
use Imoje\Payment\Util;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

/**
 * Class Checkout
 *
 * @package Imoje\Blik\Block
 */
class Checkout extends Template
{

	/**
	 * @var UrlInterface
	 */
	public $urlBuilder;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @var StoreInterface
	 */
	private $store;

	/**
	 * @var Cart
	 */
	private $cartHelper;

	/**
	 * @var ResponseFactory
	 */
	private $responseFactory;

	/**
	 * @var ProductMetadataInterface $productMetadataInterface
	 */
	private $productMetadataInterface;

	/**
	 * Checkout constructor.
	 *
	 * @param Context                  $context
	 * @param Order                    $order
	 * @param Registry                 $registry
	 * @param StoreInterface           $store
	 * @param UrlInterface             $urlBuilder
	 * @param ScopeConfigInterface     $scopeConfig
	 * @param Cart                     $cartHelper
	 * @param ResponseFactory          $responseFactory
	 * @param ProductMetadataInterface $productMetadataInterface
	 *
	 * @internal param OrderFactory $orderFactory
	 */
	public function __construct(
		Context                  $context,
		Order                    $order,
		Registry                 $registry,
		StoreInterface           $store,
		UrlInterface             $urlBuilder,
		ScopeConfigInterface     $scopeConfig,
		Cart                     $cartHelper,
		ResponseFactory          $responseFactory,
		ProductMetadataInterface $productMetadataInterface

	) {
		parent::__construct($context);
		$this->registry = $registry;
		$this->order = $order;
		$this->store = $store;
		$this->urlBuilder = $urlBuilder;
		$this->scopeConfig = $scopeConfig;
		$this->cartHelper = $cartHelper;
		$this->responseFactory = $responseFactory;
		$this->productMetadataInterface = $productMetadataInterface;

		$paramBlikCode = $this->getRequest()->getParam('blikCode');

		if($paramBlikCode) {
			$this->renderForm($paramBlikCode);
			die();
		}

		if($this->getOrderState($this->getOrderIdFromPost()) === \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {
			$this->redirectToHomePage();
			die();
		}
	}

	/**
	 * @param string $paramBlikCode
	 *
	 * @return string
	 */
	public function renderForm($paramBlikCode = '')
	{

		$writer = new Stream(BP . '/var/log/imoje.log');
		$this->logger = new Logger();
		$this->logger->addWriter($writer);

		if($paramBlikCode
			&& (!is_numeric($_POST['blikCode'])
				|| strlen($_POST['blikCode']) != 6)) {

			$this->logger->info('Blik code is invalid');

			echo json_encode([
				'status' => false,
			]);
			die();
		}

		$imojeApi = new Api(
			$this->getConfigValue('payment/imoje_pbl/authorization_token'),
			$this->getConfigValue('payment/imoje_pbl/merchant_id'),
			$this->getConfigValue('payment/imoje_pbl/service_id'),
			$this->getConfigValue('payment/imoje_pbl/sandbox')
				? Util::ENVIRONMENT_SANDBOX
				: Util::ENVIRONMENT_PRODUCTION
		);

		$order = $this->getOrderObject($this->getOrderId());

		$prepareData = $imojeApi->prepareData(
			Util::convertAmountToFractional($order->getGrandTotal()),
			$order->getOrderCurrency()->getCode(),
			$order->getIncrementId(),
			Util::getPaymentMethod('blik'),
			Util::getPaymentMethodCode('blik'),
			$this->urlBuilder->getUrl('checkout/onepage/success/'),
			$this->urlBuilder->getUrl('checkout/onepage/failure/'),
			$order->getBillingAddress()->getFirstname(),
			$order->getBillingAddress()->getLastname(),
			$order->getCustomerEmail(),
			Api::TRANSACTION_TYPE_SALE,
			'',
			$paramBlikCode
		);

		$transaction = $imojeApi->createTransaction($prepareData);

		$order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
		$order->save();;

		if($transaction['success']) {

			if($paramBlikCode) {
				echo json_encode([
					'status' => $transaction['success'],
				]);
				die();
			}

			return Util::createOrderForm(
				Api::parseStringToArray($transaction['body']),
				__('Continue'),
				$transaction['body']['action']['url']
			);
		}

		if(isset($transaction['data']['body']) && $transaction['data']['body']) {
			$this->logger->info('Transaction could not be initialized, error: ' . $transaction['data']['body']);
		}

		if($paramBlikCode) {
			echo json_encode([
				'status' => false,
			]);
			die();
		}

		$this->redirectToHomePage();
		die();
	}

	/**
	 * @return integer
	 */
	public function getOrderId()
	{
		return $this->registry->registry('orderId');
	}

	/**
	 * Render form with order information prepared for Payment Page
	 *
	 * @param string $id
	 *
	 * @return bool|\Imoje\Blik\Model\Sales\Order
	 */
	public function getOrderObject($id = '')
	{
		if(empty($id)) {
			$id = $this->getOrderId();
		}

		return $this->order->loadOrderById($id);
	}

	/**
	 * That function redirect to homepage
	 *
	 * @return mixed
	 */
	public function redirectToHomePage()
	{
		return $this->responseFactory->create()->setRedirect($this->urlBuilder->getUrl())->sendResponse(); //Redirect to cms page
	}

	/**
	 * @param string $orderId
	 *
	 * @return mixed
	 */
	public function getOrderState($orderId = '')
	{

		return $this->getOrderObject($orderId)->getState();
	}

	/**
	 * @return string
	 */
	public function getOrderIdFromPost()
	{
		$id = '';
		if(isset($_POST['orderId'])) {
			$id = $_POST['orderId'];
		}

		return $id;
	}

	/**
	 * @return string
	 */
	public function isCodeField()
	{
		return $this->getConfigValue('payment/imoje_blik/blik_code_field');
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	private function getConfigValue($value)
	{
		return $this->scopeConfig->getValue($value, ScopeInterface::SCOPE_STORE);
	}
}
