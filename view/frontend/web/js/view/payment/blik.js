/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'imoje_blik',
                component: 'Imoje_Blik/js/view/payment/method-renderer/blik'
            }
        );
        return Component.extend({});
    }
);
